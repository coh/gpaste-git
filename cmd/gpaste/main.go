package main

// Copyright 2015 Clemens-O. Hoppe.
// SPDX-License-Identifier: AGPL-3.0

import (
	"bytes"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"html/template"
	"io"
	"log"
	"net/http"
	"sort"
	"strings"

	"github.com/alecthomas/chroma"
	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
	_ "github.com/mattn/go-sqlite3"
)

type Paste struct {
	Id   string
	Data string
	Lang string
}

type HighlightedPaste struct {
	Id   string
	Data template.HTML
	Lang string
}

var db *sql.DB
var allChromaLanguages []string

func get_post_data(req *http.Request) (*Paste, error) {
	segments := strings.Split(req.URL.Path, "/")
	data_id := segments[len(segments)-1]

	var data string = ""
	var lang string = "plaintext"
	err := db.Ping()
	if err != nil {
		return &Paste{"", data, lang}, err
	}
	res := db.QueryRow("SELECT data, lang FROM pastes WHERE id=?", data_id)
	err = res.Scan(&data, &lang)

	return &Paste{data_id, data, lang}, err
}

const chromaHighlightStyle = "fruity"

func highlight(paste *Paste) *HighlightedPaste {
	lexer := lexers.Get(paste.Lang)
	if lexer == nil {
		lexer = lexers.Fallback
	}
	lexer = chroma.Coalesce(lexer)

	h := html.New(html.WithLineNumbers(), html.LineNumbersInTable(), html.WithClasses())
	if h == nil {
		return &HighlightedPaste{paste.Id, template.HTML(paste.Data), paste.Lang}
	}

	style := styles.Get(chromaHighlightStyle)
	if style == nil {
		style = styles.Fallback
	}

	iter, err := lexer.Tokenise(nil, paste.Data)
	if err != nil {
		return &HighlightedPaste{paste.Id, template.HTML(paste.Data), paste.Lang}
	}

	buffer := new(bytes.Buffer)
	h.Format(buffer, style, iter)
	css := new(bytes.Buffer)
	h.WriteCSS(css, style)
	highlighted_data := template.HTML(buffer.String() + "<style>" + css.String() + "</style>")

	return &HighlightedPaste{paste.Id,
		highlighted_data,
		paste.Lang}
}

func display_post(w http.ResponseWriter, req *http.Request) {
	paste, err := get_post_data(req)
	if err != nil {
		log.Printf("W| Failed to get post id %s: %s\n", paste.Id, err)
		http.NotFound(w, req)
	} else {
		t, err := template.ParseFiles("templates/paste.html")
		if err != nil {
			return // TODO better handling
		}

		w.Header().Set("Content-type", "text/html")
		t.Execute(w, highlight(paste))
	}
}

func content_type_for_lang(lang string) string {
	return lang //TODO
}

func raw_post(w http.ResponseWriter, req *http.Request) {
	paste, err := get_post_data(req)
	if err != nil {
		log.Printf("W| Failed to get post id %s: %s\n", paste.Id, err)
		http.NotFound(w, req)
	} else {
		w.Header().Set("Content-type", content_type_for_lang(paste.Lang))
		io.Copy(w, strings.NewReader(paste.Data))
	}
}

func render_index(w http.ResponseWriter, req *http.Request) {
	lang_names := lexers.Names(false)
	sort.Strings(lang_names)
	t, err := template.ParseFiles("templates/index.html")
	if err != nil {
		return //TODO better handling
	} else {
		t.Execute(w, allChromaLanguages)
	}
}

func generate_id() (string, error) {
	c := 24 // bytes
	data := make([]byte, c)
	_, err := rand.Read(data)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(data), err
}

func store_new_paste(w http.ResponseWriter, req *http.Request) (string, error) {
	err := db.Ping()
	if err != nil {
		return "", err
	}
	tx, err := db.Begin()
	if err != nil {
		return "", err
	}
	paste_id, err := generate_id()
	if err != nil {
		return "", err
	}
	err = req.ParseMultipartForm(16 << 20)
	if err != nil {
		return "", err
	}
	data, ok := req.MultipartForm.Value["data"]
	if !ok {
		//TODO
		return "", err
	}
	lang, ok := req.MultipartForm.Value["lang"]
	if !ok {
		//TODO
		return "", err
	}
	_, err = tx.Exec("INSERT INTO pastes(id, data, lang) VALUES(?, ?, ?)", paste_id, data[0], lang[0])
	if err != nil {
		return "", err
	}
	err = tx.Commit()
	return paste_id, err
}

func buildurl(req *http.Request, id string) string {
	return req.URL.Scheme + req.URL.Host + "/post/" + id
}

func index(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		render_index(w, req)
	} else if req.Method == "POST" {
		paste_id, err := store_new_paste(w, req)
		if err != nil {
			log.Printf("E| failed to store new paste: %s\n", err)
		}
		http.Redirect(w, req, buildurl(req, paste_id), http.StatusTemporaryRedirect)
	}
}

func create_db_tables(tx *sql.Tx, err error) error {
	if err != nil {
		return err
	}
	_, err = tx.Exec(`CREATE TABLE IF NOT EXISTS pastes (
	     id TEXT PRIMARY KEY,
	     data TEXT,
	     lang TEXT)`)
	if err != nil {
		return err
	}
	return tx.Commit()
}

func listAllChromaLanguages() {
	allChromaLanguages = lexers.Names(false)
	sort.Strings(allChromaLanguages)
}

func main() {
	const (
		useTLS    = false
		listenURL = ":8080"
		certFile  = "foo.cert"
		keyFile   = "foo.key"
	)

	var err error //ugly hack that needs to be here
	db, err = sql.Open("sqlite3", "pastes.sqlite")
	if err != nil {
		log.Fatal("Failed to open storage backend 'pastes.sqlite':", err)
	}

	err = create_db_tables(db.Begin())
	if err != nil {
		log.Fatal(err)
	}

	listAllChromaLanguages()

	http.HandleFunc("/post/", display_post)
	http.HandleFunc("/raw/", raw_post)
	http.Handle("/s/", http.StripPrefix("/s/", http.FileServer(http.Dir("static"))))
	http.HandleFunc("/", index)
	if useTLS {
		log.Fatal(http.ListenAndServeTLS(listenURL, certFile, keyFile, nil))
	} else {
		log.Fatal(http.ListenAndServe(listenURL, nil))
	}
}
